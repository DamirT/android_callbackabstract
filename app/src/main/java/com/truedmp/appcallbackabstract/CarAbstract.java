package com.truedmp.appcallbackabstract;

public abstract class CarAbstract {

    public abstract int getHs();

    public abstract String getColor();

    public abstract String getType();

}
