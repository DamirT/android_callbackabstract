package com.truedmp.appcallbackabstract;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Citroen citroen;
    Fiat fiat;
    Opel opel;

    TextView carHs;
    TextView carColor;
    TextView carType;

    Button citroenButton;
    Button fiatButton;
    Button opelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        carHs = findViewById(R.id.car_hs);
        carColor = findViewById(R.id.car_color);
        carType = findViewById(R.id.car_type);

        citroenButton = findViewById(R.id.citroen_button);
        fiatButton = findViewById(R.id.fiat_button);
        opelButton = findViewById(R.id.opel_button);

        citroenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citroen = new Citroen(100, "Blue", "Limusine");
                init(citroen);
            }
        });
        fiatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fiat = new Fiat(90, "Black", "Hatchback");
                init(fiat);
            }
        });
        opelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opel = new Opel(80, "White", "Hatchback");
                init(opel);
            }
        });
    }

    public void init(CarAbstract carAbstract) {
        if(carAbstract instanceof Citroen) {
            carHs.setText("" + carAbstract.getHs());
            carColor.setText(carAbstract.getColor());
            carType.setText(carAbstract.getType());
            return;
        }
        if(carAbstract instanceof Fiat) {
            carHs.setText("" + carAbstract.getHs());
            carColor.setText(carAbstract.getColor());
            carType.setText(carAbstract.getType());
            return;
        }
        if(carAbstract instanceof Opel) {
            carHs.setText( "" + carAbstract.getHs());
            carColor.setText(carAbstract.getColor());
            carType.setText(carAbstract.getType());
            return;
        }
    }

}