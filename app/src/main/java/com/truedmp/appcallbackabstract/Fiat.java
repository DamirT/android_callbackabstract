package com.truedmp.appcallbackabstract;

public class Fiat extends CarAbstract{

    private int horsePower;
    private String color;
    private String type;

    public Fiat(int horsePower, String color, String type) {
        this.horsePower = horsePower;
        this.color = color;
        this.type = type;
    }

    @Override
    public int getHs() {
        return horsePower;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getType() {
        return type;
    }
}
